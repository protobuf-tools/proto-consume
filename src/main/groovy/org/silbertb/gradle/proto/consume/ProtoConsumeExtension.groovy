package org.silbertb.gradle.proto.consume

import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.util.internal.ConfigureUtil

class ProtoConsumeExtension {

    private Project project
    private Configuration conf = new Configuration()

    ProtoConsumeExtension(Project project) {
        this.project = project
    }

    void config(Closure closure) {
        ConfigureUtil.configure(closure, conf)
        configureProtobufPlugin()
    }

    protected Configuration getConf() {
        return conf
    }

    private void configureProtobufPlugin() {
        def generatedBaseDir = "$project.projectDir/src/generated/proto"
        if(conf.isGenerateJava()) {
            JavaPluginExtension javaExtension = project.getExtensions().getByType(JavaPluginExtension.class);
            SourceSetContainer ssc = javaExtension.sourceSets

            def srcDir = "$generatedBaseDir/main/java"
            ssc.main.java.srcDir srcDir
            if(conf.isUseGrpc()) {
                ssc.main.java.srcDir "$generatedBaseDir/main/grpc/"
            }
            if(conf.isUseValidation()) {
                ssc.main.java.srcDir "$generatedBaseDir/main/javapgv/"
            }
        }

        project.protobuf {
            protoc {
                // Download from repositories
                artifact = 'com.google.protobuf:protoc:3.17.3'
            }

            generatedFilesBaseDir = generatedBaseDir

            plugins {
                if(conf.isUseValidation()) {
                    javapgv {
                        artifact = 'io.envoyproxy.protoc-gen-validate:protoc-gen-validate:0.6.1'
                    }
                }

                if(conf.isUseGrpc() && conf.isGenerateJava()) {
                    grpc {
                        artifact = 'io.grpc:protoc-gen-grpc-java:1.39.0'
                    }

                }
            }

            generateProtoTasks {
                all()*.plugins {
                    javapgv {
                        for (String lang : conf.generateLanguages) {
                            def javapvgLang = conf.getJavapgvLanguag(lang)
                            if(javapvgLang != null) {
                                option "lang=$javapvgLang"
                            }
                        }
                    }

                    grpc{}
                }
            }

            generateProtoTasks {
                all().each {
                    task ->
                        task.builtins {
                            for(String lang : conf.generateLanguages) {
                                switch(lang) {
                                    case "java":
                                        java{}
                                        break
                                    case "cpp":
                                        cpp{}
                                        break
                                    case "python":
                                        python{}
                                        break
                                    case "js":
                                        js{}
                                        break
                                    default:
                                        throw new RuntimeException("Unsupported language: " + lang)
                                }
                            }

                            if(!conf.isGenerateJava()) {
                                remove java
                            }

                            if(conf.isGenerateDescriptorSet()) {
                                task.generateDescriptorSet = true
                                task.descriptorSetOptions.includeImports = true
                            }

                            task.dependsOn("handleProtoDepsConflicts")
                            project.tasks.lastProtoTask.dependsOn task
                        }
                }
            }

            generateProtoTasks {
                //a hack to ensure dependencies
                project.tasks.extractIncludeProto.dependsOn("firstProtoTask")
                project.tasks.extractProto.dependsOn("firstProtoTask")
            }
        }

    }
}
