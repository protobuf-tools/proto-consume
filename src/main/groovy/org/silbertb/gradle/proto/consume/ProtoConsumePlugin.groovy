package org.silbertb.gradle.proto.consume

import groovy.io.FileType
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency

class ProtoConsumePlugin implements Plugin<Project> {

    private final String taskGroup = "Proto Consume"
    @Override
    void apply(Project project) {

        //This plugin uses the google protobuf plugin so we apply it first
        project.plugins.apply('com.google.protobuf')

        //Create a configuration extension which wraps google protobuf plugin configuration extension
        ProtoConsumeExtension extension = project.extensions.create("protoConsume", ProtoConsumeExtension, project)

        //Add dependency packages to the project code
        addDependenciesForJavaProject(project, extension)

        //extracted-include-protos contains proto definitions which are already compiled by their own projects
        //so they shouldn't be compiled again. extracted-protos contains proto definition extracted by projects that don't have them
        //compiled. In some cases the same definition may appear in both directories since the same dependency arrive from different sources.
        //In that case the definitions should not be compiled and therefore are deleted from extracted-protos.
        project.task("handleProtoDepsConflicts") {
            setGroup(taskGroup)
            mustRunAfter("extractIncludeProto")
            mustRunAfter("extractProto")

            doLast {
                def dir = new File("$project.buildDir/extracted-include-protos")
                dir.eachFileRecurse(FileType.FILES) { file ->
                    def relativePath = dir.relativePath(file)
                    def f = new File("$project.buildDir/extracted-protos", relativePath)
                    f.delete()
                }
            }
        }

        project.task("firstProtoTask") {
           setGroup(taskGroup)
        }

        project.task("lastProtoTask") {
            setGroup(taskGroup)
        }
    }

    private void addDependenciesForJavaProject(Project project, ProtoConsumeExtension extension) {
        project.afterEvaluate {
            if (extension.conf.isGenerateJava()) {
                def implementationConf = project.getConfigurations().getByName("implementation")
                if (implementationConf != null) {
                    def implementationDeps = implementationConf.getDependencies()
                    implementationDeps.add(createDep(project, 'com.google.protobuf:protobuf-java:3.17.3'))
                    if (extension.conf.isUseValidation()) {
                        implementationDeps.add(createDep(project, 'io.envoyproxy.protoc-gen-validate:pgv-java-stub:0.6.1'))
                        if (extension.conf.isUseGrpc()) {
                            implementationDeps.add(createDep(project, 'io.envoyproxy.protoc-gen-validate:pgv-java-grpc:0.6.1'))
                        }
                    }
                    if (extension.conf.isUseGrpc()) {
                        implementationDeps.add(createDep(project, 'io.grpc:grpc-all:1.39.0'))
                    }
                }
            }
        }
    }

    private Dependency createDep(Project project, String dependency) {
        return project.getDependencies().create(dependency)
    }
}
