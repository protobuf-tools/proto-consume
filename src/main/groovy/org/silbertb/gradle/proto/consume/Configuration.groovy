package org.silbertb.gradle.proto.consume

class Configuration {

    private final def supportedLanguages = ["java", "cpp", "python", "js"] as Set
    private final def javapgvSupportedLanguages = [java: "java", cpp: "cc"]

    private Set<String> generateLanguages = new HashSet<>()
    private boolean useValidation = false
    private boolean useGrpc = false
    private boolean generateDescriptorSet = false

    void generateDescriptorSet() {
        generateDescriptorSet = true;
    }

    boolean isGenerateDescriptorSet() {
        return generateDescriptorSet
    }

    void useValidation() {
        useValidation = true
    }

    boolean isUseValidation() {
        return useValidation
    }

    void useGrpc() {
        useGrpc = true
    }

    boolean isUseGrpc() {
        return useGrpc
    }

    void generateLanguage(String language) {
        if(!supportedLanguages.contains(language)) {
            throw new IllegalArgumentException(language + " is not supported by Proto Consume")
        }
        generateLanguages.add(language)
    }

    Set<String> getGenerateLanguages() {
        return generateLanguages
    }

    boolean isGenerateJava() {
        generateLanguages.contains("java")
    }

    String getJavapgvLanguag(String language) {
        return javapgvSupportedLanguages.get(language)
    }
}
