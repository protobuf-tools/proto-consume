# Proto Consume
## About the Project
This gradle plugin is aimed to consume protobuf schemas from maven repositories and generate code out of them. The other side of it, publishing the schemas, can be done using the [_Proto Publish_](https://gitlab.com/protobuf-tools/proto-publish) gradle plugin.

This plugin is merely a wrapper to [_Protobuf Plugin for Gradle_](https://github.com/google/protobuf-gradle-plugin), aimed to be easier to use on the account of flexibility.

When using schema to represent the data model and api one of the most important things is sharing it between projects. When the schema is used for code generation then it becomes important to do it when building the project. The idea is to share the schema and then generate the code rather than sharing a generated package.

## Why not schema registry?
One of the ways to do it is utilizing a schema registry. Schema registries provides more than just sharing - they can enforce conventions over the api and check for backward compatibility. 
However, there are some drawbacks for using schema registries during the build pipeline.
* There is a need to maintain a server for this repository, which require resources, dependencies on other teams and adds complexity
* When working on local machine there is still a need to communicate with the server to check the conventions and backward compatibility
* Accessing the schema registry in compile time is not always trivial. They tend to be oriented for use in runtime, for integration in message queues like kafka or pulsar.

## How to use it?
An example for usage can be found in [_Proto Service Example_](https://gitlab.com/protobuf-tools/proto-service-example) repo.  
Below is detailed explanation.

### Apply the plugin
```groovy
plugins {
    id "com.gitlab.protobuf-tools.proto-consume" version "1.0.0"
}
```

### Consume schema
Schemas are treated like any other maven artifact. The dependencies are defined in the gradle dependencies block using the keyword __protobuf__.

Example:
```groovy
dependencies {
    protobuf 'io.gitlab.protobuf-tools:example-data-model:1.0.0'
}
```

### Code Generation
Generating code out of the protobuf dependencies happens as part of the _build_, but first there is a need to specify the code language.
The generated code language is specified of the configuration block like this:
```groovy
protoConsume {
    config {
        generateLanguage("java")
    }
}
```
There can be mulitple generated languages, each should have its own generateLanguage(...) line.
The supported languages are: java, cpp, python, js

When generating grpc schemas as well there is a need to specify _useGrpc_ in the configuration block.
```groovy
protoConsume {
    config {
        generateLanguage("java")
        useGrpc()
    }
}
```

Also, if using the [_protoc-gen-validate_](https://github.com/envoyproxy/protoc-gen-validate) plugin for protobuf api validation, there is a need to specify _useValidation()_ in the configuration block as well.

It is also possible to generate a descriptorSet by adding the configuration blobk: generateDescriptorSet true.

The generated code will always be placed under the _generated_ folder.

### Order between tasks
Sometimes there is a need to do the code generation after a certain task or before a certain task. There are multiple tasks for code generation and they are defined ad-hoc so they can't be refered directly. Instead there are two tasks: _firstProtoTask_ and _lastProtoTsk_ which can be refered liked any other task, for exdample with _dependOn_ function.

## Contributing

Any contributions are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request

## License

Distributed under the APACHE License. See [LICENSE](./LICENSE) for more information.

## Contact

Barak Silbert - silbert.barak@gmail.com 

Project Link: <https://gitlab.com/protobuf-tools/proto-consume>


